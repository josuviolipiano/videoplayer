import React from 'react';
import { StyleSheet, View } from 'react-native';
import Video from 'react-native-video';
import MediaAvPlayer from 'react-native-mediaplayer-avplayer'
const App = () => {
  return (
    // <View style={styles.container}>
      <MediaAvPlayer
					style={{flex:1,zIndex:0}} 
					playerConfig={{
					  url:"https://customer-bj-mbox-25.nginxradarinstances.click/hls/cam1562.m3u8",
					  repeat: true, 
					  mute: false,
					  autostart: true
					}}
					/>
    // </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  video: {
    width: 300,
    height: 200,
  },
});

export default App;
